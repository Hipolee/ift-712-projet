# Projet de session - IFT712
Développé par Lucas Bourzier (Matricule : 20 084 329) et Alexis Lucas (Matricule : 20 084 347), dans le cadre du cours 
IFT712 à l'Université de Sherbrooke, session Automne 2020.

### Librairies nécessaires
Pour utiliser ce projet, vous devez être en possession de Python avec les librairies suivantes :
- pandas
- matplotlib.pyplot
- numpy
- sklearn

Le code a été testé avec la version 3.8 de Python.

### Comment utiliser le projet ?
Pour lancer le projet, ouvrez une invite de commande à la racine et utilisez la ligne suivante : "python main.py".
Lorsque cela est fait, l'IHM du projet vous sera affichée dans la console et vous pourrez commencer à faire vos choix.

L'IHM vous proposera de choisir les éléments ci-dessous :
- Le type de normalisation (aucune, groupement spécifique, par la moyenne, la médiane ou le min-max)
- Le modèle à utiliser (régression logistique, discriminant linéaire de Fisher, perceptron, SVM Classifier, 
réseau de neurones ou arbres de décision)
- L'application, ou non, de la formule du Z-Score sur les données.
- L'application, ou non, de l'ACP sur les données.
- Affichage, ou non, des résultats de la normalisation (avec graphiques).

Un choix supplémentaire, concernant le nombre de neurones de la couche cachée, vous sera demandé pour le 
réseau de neurones.
