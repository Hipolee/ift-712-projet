import numpy as np


def validate(model, method, x_valid, t_valid, prediction_function, show=False):
    """
    Fonction permettant de valider un modèle, selon l'ensemble de validation.
    :param model: nom du modèle à valider.
    :param method: nom de la méthode de normalisation utilisée.
    :param x_valid: ensemble de validation à utiliser.
    :param t_valid: étiquettes de classes de l'ensemble d'entraînement.
    :param prediction_function: fonction de prédiction du modèle à valider.
    :param show: si l'utilisateur souhaite voir le résultat de la validation (succès en %).
    :return: l'erreur de la validation.
    """
    errors = np.array([])
    valids = np.array([])
    for i in range(np.shape(x_valid)[0]):
        prediction = prediction_function(x_valid[i])
        errors = np.append(errors, error(t_valid[i], prediction))
        valids = np.append(valids, prediction == t_valid[i])

    err = np.mean(errors)
    valid = (np.sum(valids) / x_valid.shape[0]) * 100

    if show:
        print("[{} ({})] Succès : {}%.".format(model, method, round(valid, 2)))

    return err


def error(t, prediction):
    """
    Fonction permettant de calculer l'erreur, selon la méthode des moindres carrés.
    :param t: cible de la donnée.
    :param prediction: prédiction sur la donnée.
    :return: l'erreur de la prédiction.
    """
    return (prediction - t) ** 2
