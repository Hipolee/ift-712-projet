from sklearn.linear_model import LogisticRegressionCV


class LogisticRegression:
    """Classe du modèle de la régression logistique"""

    def __init__(self, data, method):
        """
        Constructeur du modèle.
        :param data: données à utiliser pour la classification.
        :param method: nom de la méthode de normalisation utilisée.
        """
        self.data = data
        self.clf = 0
        self.method = method
        self.model = "REGRESSION LOGISTIQUE"

        self.train(self.data.trainSample, self.data.trainLabelNumber)

    def train(self, x_train, t_train):
        """
        Méthode permettant d'entraîner le modèle de la régression logistique.
        :param x_train: ensemble d'entraînement.
        :param t_train: étiquettes de classes de l'ensemble d'entraînement.
        """
        self.clf = LogisticRegressionCV(cv=3, random_state=0, max_iter=1000)
        self.clf.fit(x_train, t_train)

    def predict(self, X):
        """
        Méthode permettant de réaliser une prédiction avec le modèle de la régression logistique entraîné.
        :param X: donnée à prédire.
        :return: le résultat de la prédiction.
        """
        X = X.reshape(1, -1)
        return self.clf.predict(X)[0]


