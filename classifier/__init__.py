from .fisher_linear_discriminant import FisherLinearDiscriminant
from .logistic_regression import LogisticRegression
from .neural_network import NeuralNetwork
from .perceptron import Perceptron
from .random_forest import RandomForest
from .svm_classification import SVMClassification