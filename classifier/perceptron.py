import numpy as np
from sklearn.linear_model import Perceptron as P
import tools


class Perceptron:
    """Classe d'un Perceptron"""

    def __init__(self, lamb, data, method):
        """
        Constructeur du modèle.
        :param data: données à utiliser pour la classification.
        :param method: nom de la méthode de normalisation utilisée.
        """
        self.data = data
        self.lamb = lamb
        self.method = method
        self.clf = None
        self.model = "PERCEPTRON"

        self.cross_validation(self.data.trainSample, self.data.trainLabelNumber)

    def cross_validation(self, x, t):
        """
        Méthode permettant de cross-valider sur le taux d'apprentissage.
        :param x: ensemble d'entraînement utilisé.
        :param t: étiquettes de classes de l'ensemble d'entraînement.
        """
        print("[{} ({})] Entrainement en cours...".format(self.model, self.method))

        # Initialisation des variables
        global_error = 100000
        temp_x = None
        temp_t = None
        global_temp_lr = None
        global_temp_x = None
        global_temp_t = None
        lr = 0.1
        lr_max = 0.5
        k = 10

        # Préparation des données pour la k-fold cross-validation.
        splitted_data = np.array_split(x, k)
        splitted_target = np.array_split(t, k)

        # Cross-validation sur le paramètre LEARNING-RATE.
        while lr < lr_max:
            temp_value_error = 100000

            # Cross-validation sur k ensemble de validation.
            for index in range(k):
                x_train = np.copy(splitted_data)
                t_train = np.copy(splitted_target)
                x_train = np.delete(x_train, index, axis=0)
                t_train = np.delete(t_train, index, axis=0)
                x_train = np.concatenate(x_train)
                t_train = np.concatenate(t_train)
                x_valid = splitted_data[index]
                t_valid = splitted_target[index]

                self.train(x_train, t_train, lr)
                error = tools.validate("PERCEPTRON", self.method, x_valid, t_valid, self.predict)

                if error < temp_value_error:  # stock le meilleur train
                    temp_value_error = error
                    temp_x = x_train
                    temp_t = t_train

            up = False
            if temp_value_error < global_error:
                up = True
                global_error = temp_value_error
                global_temp_x = temp_x
                global_temp_t = temp_t
                global_temp_lr = lr

            print_up = "Non"
            if up:
                print_up = "Oui"
            print("[{} ({})] CROSS-VALIDATION: tentative avec lr={}. Amélioration : {}."
                  .format(self.model, self.method, lr, print_up))
            lr = round(lr + 0.01, 2)

        self.train(global_temp_x, global_temp_t, global_temp_lr)
        print("[{} ({})] Entrainement terminé.".format(self.model, self.method))

    def train(self, x_train, t_train, lr=0.311):
        """
        Méthode permettant d'entraîner le modèle du perceptron.
        :param x_train: ensemble d'entraînement.
        :param t_train: étiquettes de classes de l'ensemble d'entraînement.
        """
        self.clf = P(eta0=lr, penalty='l2')
        self.clf.fit(x_train, t_train)

    def predict(self, x):
        """
        Méthode permettant de réaliser une prédiction avec le modèle du perceptron entraîné.
        :param X: donnée à prédire.
        :return: le résultat de la prédiction.
        """
        x = x.reshape(1, -1)
        return self.clf.predict(x)[0]
