import numpy as np
from sklearn.ensemble import RandomForestClassifier

import tools


class RandomForest:
    """Classe d'une Forêt Aléatoire"""

    def __init__(self, data, method):
        """
        Constructeur du modèle.
        :param data: données à utiliser pour la classification.
        :param method: nom de la méthode de normalisation utilisée.
        """
        self.data = data
        self.clf = 0
        self.method = method
        self.model = "RANDOM FOREST"

        self.cross_validation(self.data.trainSample, self.data.trainLabelNumber)

    def cross_validation(self, x, t):
        """
        Méthode permettant de cross-valider sur la profondeur maximale.
        :param x: ensemble d'entraînement utilisé.
        :param t: étiquettes de classes de l'ensemble d'entraînement.
        """
        print("[{} ({})] Entrainement en cours...".format(self.model, self.method))

        # Initialisation des variables
        global_error = 100000
        temp_x = None
        temp_t = None
        global_temp_md = None
        global_temp_x = None
        global_temp_t = None
        md = 1
        md_max = 20
        k = 10

        # Préparation des données pour la k-fold cross-validation.
        splitted_data = np.array_split(x, k)
        splitted_target = np.array_split(t, k)

        # Cross-validation sur le paramètre MAX_DEPTH.
        while md < md_max:
            temp_value_error = 100000

            # Cross-validation sur k ensemble de validation.
            for index in range(k):
                x_train = np.copy(splitted_data)
                t_train = np.copy(splitted_target)
                x_train = np.delete(x_train, index, axis=0)
                t_train = np.delete(t_train, index, axis=0)
                x_train = np.concatenate(x_train)
                t_train = np.concatenate(t_train)
                x_valid = splitted_data[index]
                t_valid = splitted_target[index]

                self.train(x_train, t_train, md)
                error = tools.validate("PERCEPTRON", self.method, x_valid, t_valid, self.predict)

                if error < temp_value_error:  # stock le meilleur train
                    temp_value_error = error
                    temp_x = x_train
                    temp_t = t_train

            up = False
            if temp_value_error < global_error:
                up = True
                global_error = temp_value_error
                global_temp_x = temp_x
                global_temp_t = temp_t
                global_temp_md = md

            print_up = "Non"
            if up:
                print_up = "Oui"
            print("[{} ({})] CROSS-VALIDATION: tentative avec max_depth={}. Amélioration : {}."
                  .format(self.model, self.method, md, print_up))
            md = md + 1

    def train(self, x_train, t_train, max_d):
        """
        Méthode permettant d'entraîner le modèle de la forêt aléatoire.
        :param x_train: ensemble d'entraînement.
        :param t_train: étiquettes de classes de l'ensemble d'entraînement.
        :param max_d: profondeur maximale de l'arbre.
        """
        self.clf = RandomForestClassifier(max_depth=max_d, random_state=0)
        self.clf.fit(x_train, t_train)

    def predict(self, X):
        """
        Méthode permettant de réaliser une prédiction avec le modèle de la forêt aléatoire entraîné.
        :param X: donnée à prédire.
        :return: le résultat de la prédiction.
        """
        X = X.reshape(1, -1)
        return self.clf.predict(X)[0]