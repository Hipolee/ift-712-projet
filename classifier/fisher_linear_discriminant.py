from sklearn.discriminant_analysis import LinearDiscriminantAnalysis


class FisherLinearDiscriminant:
    """Classe du modèle Discriminant Linéaire de Fisher"""

    def __init__(self, data, method):
        """
        Constructeur du modèle.
        :param data: données à utiliser pour la classification.
        :param method: nom de la méthode de normalisation utilisée.
        """
        self.data = data
        self.clf = 0
        self.method = method
        self.model = "DISCRIMINANT LINEAIRE DE FISHER"

        self.train(self.data.trainSample, self.data.trainLabelNumber)

    def train(self, x_train, t_train):
        """
        Méthode permettant d'entraîner le modèle du discriminant linéaire de Fisher.
        :param x_train: ensemble d'entraînement.
        :param t_train: étiquettes de classes de l'ensemble d'entraînement.
        """
        self.clf = LinearDiscriminantAnalysis()
        self.clf.fit(x_train, t_train)

    def predict(self, X):
        """
        Méthode permettant de réaliser une prédiction avec le modèle du discriminant linéaire de Fisher entraîné.
        :param X: donnée à prédire.
        :return: le résultat de la prédiction.
        """
        X = X.reshape(1, -1)
        return self.clf.predict(X)[0]