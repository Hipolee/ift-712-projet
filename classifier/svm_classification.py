from sklearn.svm import SVC


class SVMClassification:
    """Classe du modèle SVM Classification"""

    def __init__(self, data, method):
        """
        Constructeur du modèle.
        :param data: données à utiliser pour la classification.
        :param method: nom de la méthode de normalisation utilisée.
        """
        self.method = method
        self.clf = None
        self.data = data
        self.model = "SVM CLASSIFICATION"

        self.train(self.data.trainSample, self.data.trainLabelNumber)

    def train(self, x_train, t_train):
        """
        Méthode permettant d'entraîner le modèle de SVM Classification.
        :param x_train: ensemble d'entraînement.
        :param t_train: étiquettes de classes de l'ensemble d'entraînement.
        """
        self.clf = SVC(kernel='rbf')
        self.clf.fit(x_train, t_train)

    def predict(self, X):
        """
        Méthode permettant de réaliser une prédiction avec le modèle SVM Classification entraîné.
        :param X: donnée à prédire.
        :return: le résultat de la prédiction.
        """
        X = X.reshape(1, -1)
        return self.clf.predict(X)[0]