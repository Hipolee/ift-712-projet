import numpy as np


class Data:
    """Classe contenant les données pour l'entraînement et la validation des modèles."""

    def __init__(self):
        self.content = None # Ensemble des données.
        self.labels = None # Ensemble des étiquettes de classes.
        self.labelsOneHot = None # Etiquettes de classes en vecteur One-Hot.
        self.labelsNumber = None # Etiquettes de classes en valeur numérique.
        self.classes = None # Nom des différentes classes.
        self.classesOneHot = None # Vecteur One-Hot correspondant aux noms des classes.
        self.classesNumber = None # Vecteur d'entiers correspondant aux noms des classes.
        self.normalizedData = np.array([]) # Données enregistrées après normalisation.
        self.margins = np.array([]) # Données concernant les marges.
        self.shapes = np.array([]) # Données concernant les dimensions.
        self.textures = np.array([]) # Données concernant les textures.
        self.trainSample = np.array([]) # Ensemble d'entraînement utilisé.
        self.testSample = np.array([]) # Ensemble de test utilisé.
        self.validationSample = np.array([]) # Ensemble de validation utilisé.
        self.trainLabel = np.array([]) # Etiquettes de l'ensemble d'entraînement.
        self.testLabel = np.array([]) # Etiquettes de l'ensemble de test.
        self.validationLabel = np.array([]) # Etiquettes de l'ensemble de validation.
        self.trainLabelOneHot = np.array([]) # Etiquettes de l'ensemble d'entraînement sous forme de vecteur One-Hot.
        self.trainLabelNumber = np.array([]) # Etiquettes de l'ensemble d'entraînement sous forme d'entiers.
        self.testLabelOneHot = np.array([]) # Etiquettes de l'ensemble de test sous forme de vecteur One-Hot.
        self.testLabelNumber = np.array([]) # Etiquettes de l'ensemble de test sous forme d'entiers.
        self.validationLabelOneHot = np.array([]) # Etiquettes de l'ensemble de validation sous forme de vecteur One-Hot.
        self.validationLabelNumber = np.array([]) # Etiquettes de l'ensemble de validation sous forme d'entiers.

        # Infos
        self.nb_class = None # Information sur le nombre de classes dans la base de données.
        self.nb_elements = None # Information sur le nombre d'éléments (vecteurs de données) dans la base.
        self.dimension = None # Information sur le nombre de features.
