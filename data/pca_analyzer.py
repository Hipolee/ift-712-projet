from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import numpy as np


class PcaAnalyzer:
    """Classe utilisée pour analyser et transformer les données,
    selon la méthode de l'analyse par composantes principales."""

    def __init__(self):
        """
        Constructeur de la classe PcaAnalyzer.
        """
        self.variance = None
        self.acp = PCA(svd_solver='full')
        self.sc = StandardScaler()
        self.data = None
        self.pc = None

    def pca(self):
        """
        Application de l'ACP sur les données de la classe.
        """
        if self.data is not None:
            self.pc = self.acp.fit_transform(self.data)
            self.variance = self.acp.explained_variance_

    def variance_graph(self, method):
        """
        Affichage de la variance et du graphique de la variance expliquée de l'ACP.
        :param method: méthode de normalisation.
        """
        print("----- ACP -----")
        print("Variance de l'ACP :", self.variance)
        p = self.data.shape[1]
        plt.plot(np.arange(1, p + 1), np.cumsum(self.acp.explained_variance_))
        plt.title("Variance expliquée vs. # de facteurs : " + method)
        plt.ylabel("Somme de la variance expliquée")
        plt.xlabel("Nombre de facteurs")
        plt.show()
