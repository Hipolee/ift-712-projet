import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import data

# Normalization method labels.
ALL_FEATURES = "Toutes les features"
ALL_FEATURES_GROUPED = "Features groupées par 3"
MEANS = "Moyennes"
MEDIANS = "Médianes"
MAX_MIN = "Max - Min"


class DataManager:
    """Classe contenant les différents outils pour le pré-traitement des données."""

    def __init__(self, train_file, train_size, validation_size,
                 method=ALL_FEATURES, apply_z_score=False, apply_pca=False, show_variance=False,
                 show_graphic_data=False, print_result=False):
        """
        Constructeur de DataManager permettant le traitement des données. Les méthodes de traitement seront appelées
        en fonction des paramètres choisis.
        :param train_file: chemin vers le fichier contenant les données d'entraînement.
        :param train_size: taille de l'ensemble d'entraînement choisie.
        :param validation_size: taille de l'ensemble de validation choisie.
        :param method: méthode de normalisation choisie.
        :param apply_z_score: s'il faut appliquer le Z-SCORE sur les données avant la normalisation.
        :param apply_pca: s'il faut appliquer l'ACP après la normalisation.
        :param show_variance: si l'utilisateur souhaite étudier la variance.
        :param show_graphic_data: si l'utilisateur souhaite étudier les graphiques. Cela n'est pas possible pour les
        dimensions supérieures à 3.
        :param print_result: si l'utilisateur souhaite voir les résultats de la normalisation (tableau, variance, ...).
        """

        self.data = data.Data()
        self.analyse = data.PcaAnalyzer()
        self.data.content = pd.read_csv(train_file, sep=",")
        self.data.labels = np.array(pd.DataFrame(self.data.content, columns=['species']).values.tolist())

        for i in range(1, 65):
            self.data.margins = np.append(self.data.margins,
                                          np.array(pd.DataFrame(self.data.content,
                                                                columns=['margin' + str(i)]).values.tolist()).T)

        for i in range(1, 65):
            self.data.shapes = np.append(self.data.shapes,
                                         np.array(pd.DataFrame(self.data.content,
                                                               columns=['shape' + str(i)]).values.tolist()).T)

        for i in range(1, 65):
            self.data.textures = np.append(self.data.textures,
                                           np.array(pd.DataFrame(self.data.content,
                                                                 columns=['texture' + str(i)]).values.tolist()).T)

        self.data.margins = self.data.margins.reshape((64, np.shape(self.data.labels)[0])).T
        self.data.shapes = self.data.shapes.reshape((64, np.shape(self.data.labels)[0])).T
        self.data.textures = self.data.textures.reshape((64, np.shape(self.data.labels)[0])).T

        self.convert_to_one_hot_and_numbers()

        print("[DATA ({})] Traitement des données...".format(method))

        # Data normalization
        if apply_z_score:
            self.set_data_zscore()  # Application du Z-Score sur les données.

        if method == ALL_FEATURES:
            self.get_data(apply_pca, show_variance, print_result)

        elif method == ALL_FEATURES_GROUPED:
            self.get_data_grouped_by_three(apply_pca, show_variance, print_result)

        elif method == MEANS:
            self.get_data_means(apply_pca, show_variance, show_graphic_data, print_result)

        elif method == MEDIANS:
            self.get_data_medians(apply_pca, show_variance, show_graphic_data, print_result)

        elif method == MAX_MIN:
            self.get_data_min_max(apply_pca, show_variance, show_graphic_data, print_result)

        self.split_data(train_size, validation_size)
        print("[DATA ({})] Traitement terminé.".format(method))

    def get_data(self, apply_pca, show_variance, print_result):
        """
        Méthode permettant de récupérer l'ensemble des données originales.
        :param apply_pca: si l'utilisateur souhaite appliquer l'ACP ou non sur ces données.
        :param show_variance: si l'utilisateur souhaite étudier la variance des données.
        :param print_result: si l'utilisateur souhaite voir les résultats de la normalisation (tableau, variance, ...).
        """
        self.data.normalizedData = np.concatenate((self.data.margins.T, self.data.shapes.T, self.data.textures.T)).T
        self.apply_pca(apply_pca, show_variance, print_result, False, ALL_FEATURES)


    def get_data_grouped_by_three(self, apply_pca, show_variance, print_result):
        """
        Méthode permettant de récupérer les données groupées par catégorie (marge, dimension, texture).
        :param apply_pca: si l'utilisateur souhaite appliquer l'ACP ou non sur ces données.
        :param show_variance: si l'utilisateur souhaite étudier la variance des données.
        :param print_result: si l'utilisateur souhaite voir les résultats de la normalisation (tableau, variance, ...).
        """
        self.data.normalizedData = np.column_stack((self.data.margins.T, self.data.shapes.T, self.data.textures.T)).T
        self.apply_pca(apply_pca, show_variance, print_result, False, ALL_FEATURES_GROUPED)

    def get_data_means(self, apply_pca, show_variance, show_graphic_data, print_result):
        """
        Méthode permettant de normaliser les données en utilisant la moyenne.
        :param apply_pca: si l'utilisateur souhaite appliquer l'ACP ou non sur ces données.
        :param show_variance: si l'utilisateur souhaite étudier la variance des données.
        :param show_graphic_data: si l'utilisateur souhaite étudier les graphiques correspondants aux données.
        :param print_result: si l'utilisateur souhaite voir les résultats de la normalisation (tableau, variance, ...).
        """
        meanstm = np.array([])
        meansts = np.array([])
        meanstt = np.array([])

        for i in range(0, np.size(self.data.labels)):
            meanstm = np.append(meanstm, np.mean(self.data.margins[i]))
            meansts = np.append(meansts, np.mean(self.data.shapes[i]))
            meanstt = np.append(meanstt, np.mean(self.data.textures[i]))

        self.data.normalizedData = np.column_stack((meanstm.T, meansts.T, meanstt.T))
        self.apply_pca(apply_pca, show_variance, print_result, show_graphic_data, MEANS)

    def get_data_medians(self, apply_pca, show_variance, show_graphic_data, print_result):
        """
        Méthode permettant de normaliser les données en utilisant la médiane.
        :param apply_pca: si l'utilisateur souhaite appliquer l'ACP ou non sur ces données.
        :param show_variance: si l'utilisateur souhaite étudier la variance des données.
        :param show_graphic_data: si l'utilisateur souhaite étudier les graphiques correspondants aux données.
        :param print_result: si l'utilisateur souhaite voir les résultats de la normalisation (tableau, variance, ...).
        """
        medianstm = np.array([])
        mediansts = np.array([])
        medianstt = np.array([])

        for i in range(0, np.size(self.data.labels)):
            medianstm = np.append(medianstm, np.median(self.data.margins[i]))
            mediansts = np.append(mediansts, np.median(self.data.shapes[i]))
            medianstt = np.append(medianstt, np.median(self.data.textures[i]))

        self.data.normalizedData = np.column_stack((medianstm.T, mediansts.T, medianstt.T))
        self.apply_pca(apply_pca, show_variance, print_result, show_graphic_data, MEDIANS)

    def get_data_min_max(self, apply_pca, show_variance, show_graphic_data, print_result):
        """
        Méthode permettant de normaliser les données en utilisant la méthode MIN-MAX.
        :param apply_pca: si l'utilisateur souhaite appliquer l'ACP ou non sur ces données.
        :param show_variance: si l'utilisateur souhaite étudier la variance des données.
        :param show_graphic_data: si l'utilisateur souhaite étudier les graphiques correspondants aux données.
        :param print_result: si l'utilisateur souhaite voir les résultats de la normalisation (tableau, variance, ...).
        """
        minmaxtm = np.array([])
        minmaxts = np.array([])
        minmaxtt = np.array([])

        for i in range(0, np.size(self.data.labels)):
            minmaxtm = np.append(minmaxtm, (np.max(self.data.margins[i]) - np.min(self.data.margins[i])))
            minmaxts = np.append(minmaxts, (np.max(self.data.shapes[i]) - np.min(self.data.shapes[i])))
            minmaxtt = np.append(minmaxtt, (np.max(self.data.textures[i]) - np.min(self.data.textures[i])))

        self.data.normalizedData = np.column_stack((minmaxtm.T, minmaxts.T, minmaxtt.T))
        self.apply_pca(apply_pca, show_variance, print_result, show_graphic_data, MAX_MIN)

    def set_data_zscore(self):
        """
        Méthode permettant d'appliquer le Z-SCORE sur les données.
        """
        meanstm = np.array([])
        meansts = np.array([])
        meanstt = np.array([])
        stdtm = np.array([])
        stdts = np.array([])
        stdtt = np.array([])

        for i in range(0, np.size(self.data.labels)):
            meanstm = np.append(meanstm, np.mean(self.data.margins[i]))
            meansts = np.append(meansts, np.mean(self.data.shapes[i]))
            meanstt = np.append(meanstt, np.mean(self.data.textures[i]))
            stdtm = np.append(stdtm, np.std(self.data.margins[i]))
            stdts = np.append(stdts, np.std(self.data.shapes[i]))
            stdtt = np.append(stdtt, np.std(self.data.textures[i]))

        main_shape = np.shape(self.data.margins)[0]
        second_shape = np.shape(self.data.margins)[1]

        for i in range(0, main_shape):
            for j in range(0, second_shape):
                self.data.margins[i][j] = (self.data.margins[i][j] - meanstm[i]) / stdtm[i]
                self.data.shapes[i][j] = (self.data.shapes[i][j] - meansts[i]) / stdts[i]
                self.data.textures[i][j] = (self.data.textures[i][j] - meanstt[i]) / stdtt[i]

    def print_results(self, method, plot=False):
        """
        Méthode permettant d'afficher les résultats et éventuellement les graphiques si souhaités.
        :param method: nom de la méthode de normalisation.
        :param plot: si l'utilisateur souhaite voir les graphiques.
        """
        # Print results and analysis :
        print("Données, selon la méthode", method, ":")
        print("Données :", self.data.normalizedData)
        print("Variance des marges :", self.data.normalizedData[:, 0].var())
        print("Variance des dimensions :", self.data.normalizedData[:, 1].var())
        print("Variance des textures :", self.data.normalizedData[:, 2].var())

        if plot:
            x_points = self.data.normalizedData[:, 0]
            y_points = self.data.normalizedData[:, 1]
            z_points = self.data.normalizedData[:, 2]

            fig = plt.figure()
            ax = plt.axes(projection="3d")
            ax.scatter3D(x_points, y_points, z_points, c=z_points, cmap='hsv')
            plt.title(method)

            plt.show()

    def convert_to_one_hot_and_numbers(self):
        """
        Méthode permettant de convertir les étiquettes de classes en vecteur one-hot et en liste d'entiers.
        """
        self.data.classes = np.unique(self.data.labels)
        self.data.nb_class = np.size(self.data.classes)
        self.data.nb_elements = np.shape(self.data.content)[0]
        self.data.classesOneHot = np.zeros((self.data.nb_class, self.data.nb_class))
        self.data.classesNumber = np.zeros(self.data.nb_class)
        self.data.labelsOneHot = np.zeros((self.data.nb_elements, self.data.nb_class))
        self.data.labelsNumber = np.zeros(self.data.nb_elements)

        for i in range(np.size(self.data.classes)):
            self.data.classesOneHot[i][i] = 1
            self.data.classesNumber[i] = i


            for j in range(np.size(self.data.labels)):
                if self.data.labels[j] == self.data.classes[i]:
                    self.data.labelsOneHot[j] = self.data.classesOneHot[i]
                    self.data.labelsNumber[j] = self.data.classesNumber[i]

    def apply_pca(self, apply_pca, show_variance, print_result, show_graphic_data, method):
        """
        Méthode permettant d'appliquer l'ACP sur les données.
        :param apply_pca: si l'utilisateur souhaite appliquer l'ACP ou non sur ces données.
        :param show_variance: si l'utilisateur souhaite étudier la variance des données.
        :param show_graphic_data: si l'utilisateur souhaite étudier les graphiques correspondants aux données.
        :param print_result: si l'utilisateur souhaite voir les résultats de la normalisation (tableau, variance, ...).
        :param method: nom de la méthode de normalisation.
        """
        if apply_pca:
            self.analyse.data = self.data.normalizedData
            self.analyse.pca()
            self.data.normalizedData = self.analyse.pc

            if show_variance:
                self.analyse.variance_graph(method)

        if print_result:
            self.print_results(method, show_graphic_data)

    def split_data(self, train_size, validation_size):
        """
        Méthode permettant de diviser les données en un ensemble d'entraînement et un ensemble de validation.
        :param train_size: taille de l'ensemble d'entraînement.
        :param validation_size: taille de l'ensemble de validation.
        """
        start = 0
        self.data.trainLabel = self.data.labels[start:train_size]
        start += train_size
        self.data.validationLabel = self.data.labels[start:start + validation_size]

        start = 0
        self.data.trainLabelOneHot = self.data.labelsOneHot[start:train_size]
        start += train_size
        self.data.validationLabelOneHot = self.data.labelsOneHot[start:start + validation_size]

        start = 0
        self.data.trainLabelNumber = self.data.labelsNumber[start:train_size]
        start += train_size
        self.data.validationLabelNumber = self.data.labelsNumber[start:start + validation_size]

        start = 0
        self.data.trainSample = self.data.normalizedData[start:train_size]
        start += train_size
        self.data.validationSample = self.data.normalizedData[start:start + validation_size]
