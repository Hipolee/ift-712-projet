import data as d
import classifier
import os
import platform
import tools

dirname = os.path.dirname(__file__)
FILE_PATH_TRAIN = os.path.join(dirname, './species/train_all_species.csv')
FILE_PATH_TEST = os.path.join(dirname, './species/test.csv')


def cls(): os.system('cls')  # on Windows System


def clear(): os.system('clear')  # on Linux System


def main():
    """Fonction principale du projet."""
    if platform.system() == "Windows":
        cls()
    else:
        clear()

    print("#################################################################")
    print("##                                                             ##")
    print("##                  Projet de recherche IFT712                 ##")
    print("##                LUCAS Alexis et BOURZIER Lucas               ##")
    print("##                                                             ##")
    print("#################################################################")
    print("")

    run = True

    while run:

        ### IHM ###
        input_method = None
        input_model = None
        input_zscore = None
        input_pca = None
        input_details = None
        chosen_method = None
        chosen_model = None
        chosen_zscore = None
        chosen_pca = None
        chosen_details = None

        print("Choisissez une méthode de normalisation :")
        print("1 - Toutes les features.")
        print("2 - Features regroupées par 3 catégories (marge, dimension, texture).")
        print("3 - Normalisation par la moyenne.")
        print("4 - Normalisation par la médiane.")
        print("5 - Normalisation par la méthode MIN-MAX.")

        while input_method not in ["1", "2", "3", "4", "5"]:
            print("Entrez une valeur entre 1 et 5 :")
            input_method = input()

            if input_method not in ["1", "2", "3", "4", "5"]:
                print("Votre réponse ne correspond pas à la liste proposée, entre une valeur entre 1 et 5.")
            elif input_method == "1":
                chosen_method = d.ALL_FEATURES
            elif input_method == "2":
                chosen_method = d.ALL_FEATURES_GROUPED
            elif input_method == "3":
                chosen_method = d.MEANS
            elif input_method == "4":
                chosen_method = d.MEDIANS
            elif input_method == "5":
                chosen_method = d.MAX_MIN

        print("Choisissez le modèle pour la classification :")
        print("1 - Régression logistique")
        print("2 - Discriminant linéaire de Fisher")
        print("3 - Perceptron")
        print("4 - SVM Classification")
        print("5 - Réseau de neurone")
        print("6 - Forêts aléatoires pour la classification")

        while input_model not in ["1", "2", "3", "4", "5", "6"]:
            print("Entrez une valeur entre 1 et 6 :")
            input_model = input()

            if input_model not in ["1", "2", "3", "4", "5", "6"]:
                print("Votre réponse ne correspond pas à la liste proposée, entre une valeur entre 1 et 6.")
            elif input_model:
                chosen_model = int(input_model)

        while input_zscore not in ["o", "n", "O", "N"]:
            print("Application du Z-SCORE ? (O/N) :")
            input_zscore = input()

            if input_zscore not in ["o", "n", "O", "N"]:
                print("Votre réponse doit être oui (O) ou non (N).")
            else:
                chosen_zscore = (input_zscore.lower() == "o")

        while input_pca not in ["o", "n", "O", "N"]:
            print("Application de l'ACP ? (O/N) :")
            input_pca = input()

            if input_pca not in ["o", "n", "O", "N"]:
                print("Votre réponse doit être oui (O) ou non (N).")
            else:
                chosen_pca = (input_pca.lower() == "o")

        while input_details not in ["o", "n", "O", "N"]:
            print("Afficher les détails de la normalisation ? (O/N) :")
            input_details = input()

            if input_details not in ["o", "n", "O", "N"]:
                print("Votre réponse doit être oui (O) ou non (N).")
            else:
                chosen_details = (input_details.lower() == "o")

        ### Normalisation ###

        # Application de l'ACP sur toutes les données.

        data = d.DataManager(FILE_PATH_TRAIN, 792, 198, method=chosen_method, apply_z_score=chosen_zscore,
                               apply_pca=chosen_pca, show_variance=chosen_details, show_graphic_data=chosen_details, print_result=chosen_details)

        ### Classification ###
        # Utilisation de la régression logistique.
        if chosen_model == 1:
            LR = classifier.LogisticRegression(data.data, method=chosen_method)
            tools.validate(LR.model, LR.method, LR.data.validationSample,
                           LR.data.validationLabelNumber, LR.predict, show=True)

        # Utilisation du discriminant linéaire de Fisher.
        elif chosen_model == 2:
            FLD = classifier.FisherLinearDiscriminant(data.data, method=chosen_method)
            tools.validate(FLD.model, FLD.method, FLD.data.validationSample,
                           FLD.data.validationLabelNumber, FLD.predict, show=True)

        # Utilisation du Perceptron.
        elif chosen_model == 3:
            perceptron = classifier.Perceptron(1, data.data, method=chosen_method)
            tools.validate(perceptron.model, perceptron.method, perceptron.data.validationSample,
                           perceptron.data.validationLabelNumber, perceptron.predict, show=True)

        # Utilisation de la SVM Classification.
        elif chosen_model == 4:
            svm = classifier.SVMClassification(data.data, method=chosen_method)
            tools.validate(svm.model, svm.method, svm.data.validationSample,
                           svm.data.validationLabelNumber, svm.predict, show=True)

        # Utilisation d'un réseau de neurone, à savoir un Perceptron multi-couches.
        elif chosen_model == 5:
            chosen_layer = None
            while chosen_layer is None:
                print("Combien de neurones pour la couche cachée ? (entrez un entier) :")
                try:
                    chosen_layer = int(input())
                    break
                except ValueError:
                    print("Votre réponse doit être un nombre.")
                    chosen_layer = None

            network = classifier.NeuralNetwork(data.data, method=chosen_method, hidden_layers=(chosen_layer,))
            tools.validate(network.model, network.method, network.data.validationSample,
                           network.data.validationLabelNumber, network.predict, show=True)

        # Utilisation des forêts aléatoires pour la classification.
        elif chosen_model == 6:
            randf = classifier.RandomForest(data.data, method=chosen_method)
            tools.validate(randf.model, randf.method, randf.data.validationSample,
                           randf.data.validationLabelNumber, randf.predict, show=True)

        else:
            print("Une erreur est survenue.")

        print("[PROGRAMME] Exécution terminée.")

        input_end = None
        while input_end not in ["o", "n", "O", "N"]:
            print("Réaliser une autre expérience ? (O/N) :")
            input_end = input()

            if input_end not in ["o", "n", "O", "N"]:
                print("Votre réponse doit être oui (O) ou non (N).")
            else:
                run = (input_end.lower() == "o")


# Condition for python command line.
if __name__ == "__main__":
    main()
